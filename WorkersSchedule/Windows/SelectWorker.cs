﻿using GrafikSklepowy.Class;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class SelectWorker : Form
    {
        private int workerid;
        public int workerID
        {
            get => workerid;
        }
        public SelectWorker()
        {
            InitializeComponent();
            for (int i = 0; i < WorkerList.Count; i++)
            {
                if (WorkerList.GetWorker(i).IsHired)
                {
                    Worker tmp = WorkerList.GetWorker(i);
                    int j = workersGrid.Rows.Add(tmp.Name, tmp.Surname);
                    workersGrid.Rows[j].Tag = i;
                }
            }
        }

        private void workersGrid_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            workerid = (int)workersGrid.Rows[e.RowIndex].Tag;
            this.Close();
        }
    }
}
