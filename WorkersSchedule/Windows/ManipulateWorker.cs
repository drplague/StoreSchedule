﻿using System;
using System.Windows.Forms;
namespace GrafikSklepowy.Windows
{
    public partial class ManipulateWorker : Form
    {
        private Worker worker = null;
        public Worker Worker
        {
            get => worker;
        }
        public ManipulateWorker()
        {
            InitializeComponent();
        }
        public void fillData(Worker w)
        {
            WUID.Text = Convert.ToString(w.WUID);
            FormName.Text = w.Name;
            Surname.Text = w.Surname;
            Birtday.Value = w.Birthday;
            Address.Text = w.Address;
            TaxOffice.Text = w.TaxOffice;
            HireDate.Value = w.HireDate;
            HourlyRate.Value = w.HourlyRate;
            PhoneNumber.Text = w.PhoneNumber;
        }
        public void setData(Worker w)
        {
            WUID.Text = Convert.ToString(w.WUID);
            FormName.Text = w.Name;
            Surname.Text = w.Surname;
            Birtday.Value = w.Birthday;
            Address.Text = w.Address;
            TaxOffice.Text = w.TaxOffice;
            HireDate.Value = w.HireDate;
            HourlyRate.Value = w.HourlyRate;
            PhoneNumber.Text = w.PhoneNumber;
            this.Name = "Edit Worker";
        }
        public void setID(int wuid)
        {
            WUID.Text = Convert.ToString(wuid);
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(FormName.Text) || String.IsNullOrWhiteSpace(Surname.Text) || String.IsNullOrWhiteSpace(Address.Text) || String.IsNullOrWhiteSpace(TaxOffice.Text) || HourlyRate.Value == 0 || String.IsNullOrWhiteSpace(PhoneNumber.Text))
                MessageBox.Show("Fill Data!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                worker = new Worker(Convert.ToInt32(WUID.Text), FormName.Text, Surname.Text, Birtday.Value, Address.Text, TaxOffice.Text, HireDate.Value, HourlyRate.Value, PhoneNumber.Text);
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
