﻿
namespace GrafikSklepowy.Windows
{
    partial class ManipulateWorker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.WUID = new System.Windows.Forms.Label();
            this.FormName = new System.Windows.Forms.TextBox();
            this.Surname = new System.Windows.Forms.TextBox();
            this.Address = new System.Windows.Forms.TextBox();
            this.PhoneNumber = new System.Windows.Forms.TextBox();
            this.HourlyRate = new System.Windows.Forms.NumericUpDown();
            this.Birtday = new System.Windows.Forms.DateTimePicker();
            this.HireDate = new System.Windows.Forms.DateTimePicker();
            this.TaxOffice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SaveButton = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.HourlyRate)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "WUID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-1956, -64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Surname";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Birthday";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Address";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 185);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Hire Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 215);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Hourly Rate";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Phone Number";
            // 
            // WUID
            // 
            this.WUID.AutoSize = true;
            this.WUID.Location = new System.Drawing.Point(311, 9);
            this.WUID.Name = "WUID";
            this.WUID.Size = new System.Drawing.Size(13, 15);
            this.WUID.TabIndex = 9;
            this.WUID.Text = "0";
            // 
            // FormName
            // 
            this.FormName.Location = new System.Drawing.Point(179, 31);
            this.FormName.Name = "FormName";
            this.FormName.Size = new System.Drawing.Size(145, 23);
            this.FormName.TabIndex = 10;
            // 
            // Surname
            // 
            this.Surname.Location = new System.Drawing.Point(179, 61);
            this.Surname.Name = "Surname";
            this.Surname.Size = new System.Drawing.Size(145, 23);
            this.Surname.TabIndex = 11;
            // 
            // Address
            // 
            this.Address.Location = new System.Drawing.Point(179, 121);
            this.Address.Name = "Address";
            this.Address.Size = new System.Drawing.Size(145, 23);
            this.Address.TabIndex = 12;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.Location = new System.Drawing.Point(179, 241);
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.Size = new System.Drawing.Size(145, 23);
            this.PhoneNumber.TabIndex = 13;
            // 
            // HourlyRate
            // 
            this.HourlyRate.DecimalPlaces = 2;
            this.HourlyRate.Location = new System.Drawing.Point(179, 211);
            this.HourlyRate.Name = "HourlyRate";
            this.HourlyRate.Size = new System.Drawing.Size(145, 23);
            this.HourlyRate.TabIndex = 14;
            // 
            // Birtday
            // 
            this.Birtday.Location = new System.Drawing.Point(179, 91);
            this.Birtday.Name = "Birtday";
            this.Birtday.Size = new System.Drawing.Size(145, 23);
            this.Birtday.TabIndex = 15;
            // 
            // HireDate
            // 
            this.HireDate.Location = new System.Drawing.Point(179, 181);
            this.HireDate.Name = "HireDate";
            this.HireDate.Size = new System.Drawing.Size(145, 23);
            this.HireDate.TabIndex = 16;
            // 
            // TaxOffice
            // 
            this.TaxOffice.Location = new System.Drawing.Point(179, 151);
            this.TaxOffice.Name = "TaxOffice";
            this.TaxOffice.Size = new System.Drawing.Size(145, 23);
            this.TaxOffice.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Tax Office";
            // 
            // SaveButton
            // 
            this.SaveButton.Location = new System.Drawing.Point(12, 270);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(312, 58);
            this.SaveButton.TabIndex = 19;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 334);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(312, 58);
            this.button2.TabIndex = 20;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ManipulateWorker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 407);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.TaxOffice);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.HireDate);
            this.Controls.Add(this.Birtday);
            this.Controls.Add(this.HourlyRate);
            this.Controls.Add(this.PhoneNumber);
            this.Controls.Add(this.Address);
            this.Controls.Add(this.Surname);
            this.Controls.Add(this.FormName);
            this.Controls.Add(this.WUID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ManipulateWorker";
            this.Text = "New Worker";
            ((System.ComponentModel.ISupportInitialize)(this.HourlyRate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label WUID;
        private System.Windows.Forms.TextBox FormName;
        private System.Windows.Forms.TextBox Surname;
        private System.Windows.Forms.TextBox Address;
        private System.Windows.Forms.TextBox PhoneNumber;
        private System.Windows.Forms.NumericUpDown HourlyRate;
        private System.Windows.Forms.DateTimePicker Birtday;
        private System.Windows.Forms.DateTimePicker HireDate;
        private System.Windows.Forms.TextBox TaxOffice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button button2;
    }
}