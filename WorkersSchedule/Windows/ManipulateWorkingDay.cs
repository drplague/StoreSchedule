﻿using System;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class ManipulateWorkingDay : Form
    {
        private WorkingDay newDay = null;
        public WorkingDay GetData
        {
            get => this.newDay;
        }
        public ManipulateWorkingDay()
        {
            InitializeComponent();
            datePicker.CustomFormat = "dd/MM/yyyy";
        }
        public void setDay(WorkingDay p)
        {
            datePicker.Value = p.Date;
            manHours.Value = p.ManHours;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            newDay = new WorkingDay(datePicker.Value, manHours.Value);
            this.Close();
        }
    }
}
