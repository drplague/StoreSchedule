﻿using GrafikSklepowy.Class;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class ManipulatePlannedDay : Form
    {
        private int workerID = -1;
        private int workingDayID = -1;
        PlannedDay newDay;
        public PlannedDay getPlannedDay
        {
            get => newDay;
        }
        public ManipulatePlannedDay()
        {
            InitializeComponent();
            date.CustomFormat = "dd/MM/yyyy";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (WorkerList.Count > 0)
            {
                SelectWorker sl = new SelectWorker();
                sl.ShowDialog();
                workerID = sl.workerID;
                button1.Text = WorkerList.GetWorker(workerID).Name + " " + WorkerList.GetWorker(workerID).Surname;
            }
        }
        public void SetData(PlannedDay p)
        {
            button1.Text = WorkerList.GetWorker(p.WorkerID).Name + " " + WorkerList.GetWorker(p.WorkerID).Surname;
            date.Value = p.Date;
            manHours.Value = p.PlannedHours;
            workerID = p.WorkerID;
            this.Name = "Planned Day " + p.Date;
            findHoursRem();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (manHours.Value != 0 && workerID >= 0)
            {
                newDay = new PlannedDay(date.Value, workerID, manHours.Value);
                this.Close();
            }
            else
            {
                MessageBox.Show("Fill form!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void date_ValueChanged(object sender, EventArgs e)
        {
            findHoursRem();
        }

        private void manHours_ValueChanged(object sender, EventArgs e)
        {
            findHoursRem();
        }
        private void findHoursRem()
        {
            decimal hours = 0;
            // if working day is planned on date
            if ((workingDayID = WorkingDayList.FindDate(date.Value.Date)) >= 0)
            {
                //show planned limit
                hours = WorkingDayList.GetWorkingDay(workingDayID).ManHours - PlannedDayList.HoursSum(date.Value.Date) - manHours.Value;

                //Check if planned day exist in list if yes the we are in edit mode
                int plannedid = 0;
                if ((plannedid = PlannedDayList.Find(date.Value.Date)) >= 0 && PlannedDayList.GetPlannedDay(plannedid).WorkerID == workerID)
                {
                    hours += PlannedDayList.GetPlannedDay(plannedid).PlannedHours;
                }
            }
            else
                hours -= manHours.Value;
            if (hours < 0)
            {
                manHours.BackColor = Color.Red;
                HoursRem.BackColor = Color.Red;
            }
            else
            {
                manHours.BackColor = DefaultBackColor;
                HoursRem.BackColor = Color.White;
            }
            HoursRem.Text = hours.ToString();
        }
    }
}
