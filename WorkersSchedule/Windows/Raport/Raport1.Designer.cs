﻿
namespace GrafikSklepowy.Windows
{
    partial class Raport1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.raportGrid = new System.Windows.Forms.DataGridView();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkerHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.raportGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // Date
            // 
            this.Date.HeaderText = "Data";
            this.Date.Name = "Date";
            // 
            // PlannedHours
            // 
            this.PlannedHours.HeaderText = "Zaplanowane Godziny";
            this.PlannedHours.Name = "PlannedHours";
            // 
            // raportGrid
            // 
            this.raportGrid.AllowUserToAddRows = false;
            this.raportGrid.AllowUserToDeleteRows = false;
            this.raportGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raportGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.dataGridViewTextBoxColumn1,
            this.WorkerHours});
            this.raportGrid.Location = new System.Drawing.Point(12, 12);
            this.raportGrid.MultiSelect = false;
            this.raportGrid.Name = "raportGrid";
            this.raportGrid.ReadOnly = true;
            this.raportGrid.RowTemplate.Height = 25;
            this.raportGrid.Size = new System.Drawing.Size(512, 426);
            this.raportGrid.TabIndex = 0;
            // 
            // Data
            // 
            this.Data.HeaderText = "Date";
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Planned Hours";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // WorkerHours
            // 
            this.WorkerHours.HeaderText = "Workers Hours";
            this.WorkerHours.Name = "WorkerHours";
            this.WorkerHours.ReadOnly = true;
            // 
            // Raport1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 450);
            this.Controls.Add(this.raportGrid);
            this.Name = "Raport1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.raportGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedHours;
        private System.Windows.Forms.DataGridView raportGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkerHours;
    }
}