﻿using GrafikSklepowy.Windows.Raport;
using System;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class SelectRaport : Form
    {
        public SelectRaport()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SelectWorker slw = new();
            slw.ShowDialog();
            Raport2 rp2 = new(slw.workerID);
            this.Hide();
            rp2.ShowDialog();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Raport1 rp1 = new();
            this.Hide();
            rp1.ShowDialog();
            this.Close();
        }
    }
}
