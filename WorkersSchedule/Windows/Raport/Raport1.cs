﻿using GrafikSklepowy.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrafikSklepowy.Windows
{
    public partial class Raport1 : Form
    {
        public Raport1()
        {
            InitializeComponent();
            for(int i = 0; i < WorkingDayList.Count; i++)
            {
                decimal WorkingHours = 0;
                for(int j = 0; j < PlannedDayList.Count; j++)
                {
                    if(PlannedDayList.GetPlannedDay(j).Date == WorkingDayList.GetWorkingDay(i).Date)
                    {
                        WorkingHours += PlannedDayList.GetPlannedDay(j).PlannedHours;
                    }
                }
                if(WorkingDayList.GetWorkingDay(i).ManHours != WorkingHours)
                {
                    int row = raportGrid.Rows.Add(WorkingDayList.GetWorkingDay(i).Date, WorkingDayList.GetWorkingDay(i).ManHours, WorkingHours);
                }
            }
        }
    }
}
