﻿
namespace GrafikSklepowy.Windows.Raport
{
    partial class Raport2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.raport2Grid = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkedHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.raport2Grid)).BeginInit();
            this.SuspendLayout();
            // 
            // raport2Grid
            // 
            this.raport2Grid.AllowUserToAddRows = false;
            this.raport2Grid.AllowUserToDeleteRows = false;
            this.raport2Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.raport2Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.PlannedHours,
            this.WorkedHours});
            this.raport2Grid.Location = new System.Drawing.Point(12, 12);
            this.raport2Grid.MultiSelect = false;
            this.raport2Grid.Name = "raport2Grid";
            this.raport2Grid.ReadOnly = true;
            this.raport2Grid.RowTemplate.Height = 25;
            this.raport2Grid.Size = new System.Drawing.Size(529, 426);
            this.raport2Grid.TabIndex = 0;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            // 
            // PlannedHours
            // 
            this.PlannedHours.HeaderText = "Planned Hours";
            this.PlannedHours.Name = "PlannedHours";
            this.PlannedHours.ReadOnly = true;
            // 
            // WorkedHours
            // 
            this.WorkedHours.HeaderText = "Worked Hours";
            this.WorkedHours.Name = "WorkedHours";
            this.WorkedHours.ReadOnly = true;
            // 
            // Raport2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 450);
            this.Controls.Add(this.raport2Grid);
            this.Name = "Raport2";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.raport2Grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView raport2Grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedHours;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkedHours;
    }
}