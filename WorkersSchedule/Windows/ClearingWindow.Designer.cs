﻿
namespace GrafikSklepowy.Windows
{
    partial class ClearingWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.WorkerData = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PlannedDate = new System.Windows.Forms.Label();
            this.PlannedHours = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.FinishedHours = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.FinishedHours)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Worker";
            // 
            // WorkerData
            // 
            this.WorkerData.AutoSize = true;
            this.WorkerData.Location = new System.Drawing.Point(290, 9);
            this.WorkerData.Name = "WorkerData";
            this.WorkerData.Size = new System.Drawing.Size(89, 15);
            this.WorkerData.TabIndex = 1;
            this.WorkerData.Text = "Name Surname";
            this.WorkerData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Planned Hours";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Date";
            // 
            // PlannedDate
            // 
            this.PlannedDate.AutoSize = true;
            this.PlannedDate.Location = new System.Drawing.Point(292, 40);
            this.PlannedDate.Name = "PlannedDate";
            this.PlannedDate.Size = new System.Drawing.Size(81, 15);
            this.PlannedDate.TabIndex = 4;
            this.PlannedDate.Text = "dd/MM/YYYY";
            this.PlannedDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PlannedHours
            // 
            this.PlannedHours.AutoSize = true;
            this.PlannedHours.Location = new System.Drawing.Point(360, 71);
            this.PlannedHours.Name = "PlannedHours";
            this.PlannedHours.Size = new System.Drawing.Size(13, 15);
            this.PlannedHours.TabIndex = 5;
            this.PlannedHours.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Finished Hours";
            // 
            // FinishedHours
            // 
            this.FinishedHours.Location = new System.Drawing.Point(290, 100);
            this.FinishedHours.Name = "FinishedHours";
            this.FinishedHours.Size = new System.Drawing.Size(83, 23);
            this.FinishedHours.TabIndex = 0;
            this.FinishedHours.ValueChanged += new System.EventHandler(this.FinishedHours_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 129);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(361, 79);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 214);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(361, 79);
            this.button2.TabIndex = 2;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ClearingWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 310);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.FinishedHours);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PlannedHours);
            this.Controls.Add(this.PlannedDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.WorkerData);
            this.Controls.Add(this.label1);
            this.Name = "ClearingWindow";
            this.Text = "Settle";
            ((System.ComponentModel.ISupportInitialize)(this.FinishedHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label WorkerData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label PlannedDate;
        private System.Windows.Forms.Label PlannedHours;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown FinishedHours;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}