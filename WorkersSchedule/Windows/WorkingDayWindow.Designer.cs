﻿
namespace GrafikSklepowy.Windows
{
    partial class WorkingDayWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.PlanNew = new System.Windows.Forms.ToolStripMenuItem();
            this.EditPlan = new System.Windows.Forms.ToolStripMenuItem();
            this.RemovePlan = new System.Windows.Forms.ToolStripMenuItem();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.workingDaysGrid = new System.Windows.Forms.DataGridView();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ManHours = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workingDaysGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PlanNew,
            this.EditPlan,
            this.RemovePlan});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(633, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // PlanNew
            // 
            this.PlanNew.Name = "PlanNew";
            this.PlanNew.Size = new System.Drawing.Size(69, 20);
            this.PlanNew.Tag = "PlanNew";
            this.PlanNew.Text = "Plan New";
            // 
            // EditPlan
            // 
            this.EditPlan.Name = "EditPlan";
            this.EditPlan.Size = new System.Drawing.Size(39, 20);
            this.EditPlan.Tag = "EditPlan";
            this.EditPlan.Text = "Edit";
            // 
            // RemovePlan
            // 
            this.RemovePlan.Name = "RemovePlan";
            this.RemovePlan.Size = new System.Drawing.Size(52, 20);
            this.RemovePlan.Tag = "RemovePlan";
            this.RemovePlan.Text = "Delete";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.CalendarDimensions = new System.Drawing.Size(1, 2);
            this.monthCalendar1.Location = new System.Drawing.Point(396, 33);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 2;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            this.monthCalendar1.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateSelected);
            // 
            // workingDaysGrid
            // 
            this.workingDaysGrid.AllowUserToAddRows = false;
            this.workingDaysGrid.AllowUserToDeleteRows = false;
            this.workingDaysGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.workingDaysGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.ManHours});
            this.workingDaysGrid.Location = new System.Drawing.Point(12, 33);
            this.workingDaysGrid.MultiSelect = false;
            this.workingDaysGrid.Name = "workingDaysGrid";
            this.workingDaysGrid.RowTemplate.Height = 25;
            this.workingDaysGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.workingDaysGrid.Size = new System.Drawing.Size(372, 309);
            this.workingDaysGrid.TabIndex = 3;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.Name = "Date";
            // 
            // ManHours
            // 
            this.ManHours.HeaderText = "Hours";
            this.ManHours.Name = "ManHours";
            // 
            // WorkingDayWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(633, 350);
            this.Controls.Add(this.workingDaysGrid);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "WorkingDayWindow";
            this.Text = "Working Days";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.workingDaysGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem PlanNew;
        private System.Windows.Forms.ToolStripMenuItem EditPlan;
        private System.Windows.Forms.ToolStripMenuItem RemovePlan;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.DataGridView workingDaysGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn ManHours;
    }
}