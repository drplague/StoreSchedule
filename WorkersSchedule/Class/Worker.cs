﻿using System;

namespace GrafikSklepowy
{
    public class Worker
    {
        private int wuid;
        private string name;
        private string surname;
        private DateTime birthday;
        private string address;
        private string taxOffice;
        private DateTime hireDate;
        private decimal hourlyRate;
        private string phoneNumber;
        private bool isHired = true;
        private decimal salary;
        public Worker(int wuid, string name, string surname, DateTime birthday, string address, string taxOffice, DateTime hireDate, decimal hourlyRate, string phoneNumber)
        {
            this.wuid = wuid;
            this.name = name;
            this.surname = surname;
            this.birthday = birthday;
            this.address = address;
            this.taxOffice = taxOffice;
            this.hireDate = hireDate;
            this.hourlyRate = hourlyRate;
            this.phoneNumber = phoneNumber;
        }
        public Worker()
        {
        }
        public int WUID
        {
            get => this.wuid;
            set => this.wuid = value;
        }
        public string Name
        {
            get => this.name;
            set => this.name = value;
        }
        public string Surname
        {
            get => this.surname;
            set => this.surname = value;
        }
        public DateTime Birthday
        {
            get => this.birthday;
            set => this.birthday = (DateTime)value;
        }
        public string Address
        {
            get => this.address;
            set => this.address = value;
        }
        public string TaxOffice
        {
            get => this.taxOffice;
            set => this.taxOffice = value;
        }
        public DateTime HireDate
        {
            get => this.hireDate;
            set => this.hireDate = (DateTime)value;
        }
        public decimal HourlyRate
        {
            get => this.hourlyRate;
            set => this.hourlyRate = (decimal)value;
        }
        public string PhoneNumber
        {
            get => this.phoneNumber;
            set => this.phoneNumber = value;
        }
        public bool IsHired
        {
            get => this.isHired;
            set => this.isHired = value;
        }
        public void Fire()
        {
            this.isHired = false;
        }
        public decimal Salary
        {
            get => this.salary;
            set => this.salary = value;
        }
    }
}
