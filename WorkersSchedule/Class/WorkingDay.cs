﻿using System;

namespace GrafikSklepowy
{
    public class WorkingDay
    {
        private DateTime date;
        private decimal manHours = 0;
        public DateTime Date
        {
            get => this.date;
            set => this.date = (DateTime)value;
        }
        public decimal ManHours
        {
            get => this.manHours;
            set => this.manHours = (decimal)value;
        }
        public WorkingDay(DateTime date, decimal manHours)
        {
            this.date = date.Date;
            this.manHours = manHours;
        }
        public WorkingDay()
        {

        }
    }
}
