﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace GrafikSklepowy.Class
{
    public static class PlannedDayList
    {
        private static List<PlannedDay> plannedDays = new();
        public static PlannedDay GetPlannedDay(int i)
        {
            return plannedDays[i];
        }
        public static void PlanNewDay(PlannedDay p)
        {
            plannedDays.Add(p);
        }
        public static void SetPlannedDay(int id, PlannedDay p)
        {
            plannedDays[id] = p;
        }
        public static int Count
        {
            get => plannedDays.Count;
        }
        public static void RemovePlan(int id)
        {
            plannedDays.RemoveAt(id);
        }
        public static int Find(DateTime date)
        {
            for (int i = 0; i < Count; i++)
            {
                if (plannedDays[i].Date == date.Date)
                {
                    return i;
                }
            }
            return -1;
        }
        public static decimal HoursSum(DateTime date)
        {
            decimal tmp = 0;
            foreach (PlannedDay p in plannedDays)
            {
                if (p.Date == date)
                {
                    tmp += p.PlannedHours;
                }
            }
            return tmp;
        }
        public static void Save()
        {
            XmlSerializer serializer = new XmlSerializer(plannedDays.GetType());
            using (TextWriter filestream = new StreamWriter("PlannedDays.xml"))
            {
                serializer.Serialize(filestream, plannedDays);
            }
        }
        public static void Load()
        {
            XmlSerializer deserialize = new XmlSerializer(plannedDays.GetType());
            if (File.Exists("PlannedDays.xml"))
            {
                using (TextReader filestream = new StreamReader("PlannedDays.xml"))
                {
                    plannedDays = (List<PlannedDay>)deserialize.Deserialize(filestream);
                }
            }
        }
    }
}
